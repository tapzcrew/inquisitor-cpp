// Copryright (C) 2021 Arthur LAURENT <arthur.laurent4@gmail.com>
// This file is subject to the license terms in the LICENSE file
// found in the top-level of this distribution

/////////// - StormKit::core - ///////////
#include <storm/core/Memory.hpp>

class Bot;
DECLARE_PTR_AND_REF(Bot)

class Inquisitor;
DECLARE_PTR_AND_REF(Inquisitor)
